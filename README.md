# loosetools

[![GitLab CI](https://gitlab.com/mkaag/loosetools/badges/master/build.svg)](https://gitlab.com/mkaag/loosetool/builds)

> Collection of loose tools

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# run unit tests
npm test
```
