import Vue from 'vue'
import App from './components/App.vue'
import Home from './components/Home.vue'
import About from './components/About.vue'
import Terms from './components/Terms.vue'
import Privacy from './components/Privacy.vue'
import Encoder from './components/tools/Encoder.vue'
import Adfs from './components/tools/Adfs.vue'
import Password from './components/tools/Password.vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

export var router = new VueRouter()

// Set up routing and match routes to components
router.map({
  '/': {
    component: Home
  },
  '/adfs': {
    component: Adfs
  },
  '/encoder': {
    component: Encoder
  },
  '/pwgen': {
    component: Password
  },
  '/about': {
    component: About
  },
  '/terms': {
    component: Terms
  },
  '/privacy': {
    component: Privacy
  }
})

// Redirect to the home route if any routes are unmatched
router.redirect({
  '*': '/'
})

router.start(App, '#app')
